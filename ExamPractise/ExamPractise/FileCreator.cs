﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ExamPractise
{
    public class FileCreator
    {
        public static void CreateFile(string newmaincurr, Currency backcourse)
        {
            StringBuilder text = new StringBuilder();
            text.AppendLine(newmaincurr);
            text.AppendLine(backcourse.Name + " " + backcourse.sell + " " + backcourse.buy);
            File.WriteAllText(newmaincurr + ".txt", text.ToString());
            Console.WriteLine(text);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamPractise
{
    public class CurrencyAdder
    {
        public static void AddCurrencies(string maincurr, Exchange bebra, string[] firstline)
        {
            for (int i = 1; i < firstline.Length; i++)
            {
                string[] str = firstline[i].Split();
                Currency newcurr = new Currency();
                newcurr.Name = str[0];
                newcurr.sell = double.Parse(str[1]);
                newcurr.buy = double.Parse(str[2]);
                bebra.Currencies.Add(newcurr);
            }
        }
    }
}

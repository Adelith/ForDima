﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ExamPractise
{   
    public class Exchange
    {
        public string Name { get; set; }
        public List<Currency> Currencies = new List<Currency>();
    }
    public class Currency
    {
      public string Name { get; set; }
      public double sell { get; set; }
      public double buy { get; set; }
    }

    public class SomeData
    {
        public string str;
        public Currency backcourse = new Currency();
    }

    public class Programm
    {
        public static string Path = @"data.txt";
        public static void Main()
        {
            var firstline = File.ReadAllLines(Path);
            string maincurr = firstline[0];
            Exchange bebra = new Exchange();
            bebra.Name = firstline[0];
            CurrencyAdder.AddCurrencies(maincurr, bebra, firstline);
            Execution.Execute(bebra, maincurr);
        }
        
        
    }
}
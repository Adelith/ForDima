﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamPractise
{
    public static class Execution
    {
        public static void Execute(Exchange bebra, string maincurr)
        {
            for (int i = 0; i < bebra.Currencies.Count; i++)
            {
                SomeData Data = new SomeData();
                Data.str = bebra.Currencies[i].Name;
                Data.backcourse.Name = maincurr;
                Data.backcourse.sell = 1 / bebra.Currencies[i].sell;
                Data.backcourse.buy = 1 / bebra.Currencies[i].buy;
                FileCreator.CreateFile(Data.str, Data.backcourse);
            }
        }
    }
}
